** Settings ***
Documentation     Robot Framework tests
...
...               Test to validate Robot Framework.

Library           Collections
Library           Process
Library           RequestsLibrary

*** Variables ***
${GITLAB_API_VERSION}   v4
${GITLAB_USERNAME}   fxs
${GITLAB_NAME}   jfxs

*** Test Cases ***
Test Robot Framework: [--version] option
    [Tags]    core
    ${result} =    When Run Process    robot   --version
    Then Should Be Equal As Integers    ${result.rc}    251
    And Should Contain    ${result.stdout}    Robot Framework

Test docker: [--version] option
    [Tags]    core
    ${result} =    When Run Process    docker   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Docker version

Test mask.sh: [--version] option
    [Tags]    core
    ${result} =    When Run Process    mask.sh   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    mask.sh, version

Test Requests library
    [Tags]    request
    ${resp}=   When GET   https://gitlab.com/api/${GITLAB_API_VERSION}/users   params=username=${GITLAB_USERNAME}   expected_status=200
    ${user}=   And Get From List   ${resp.json()}   0
    And Dictionary Should Contain Item  ${user}  name   ${GITLAB_NAME}

*** Keywords ***
