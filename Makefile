
DOCKER = docker

IMAGE_FROM  = docker:latest
IMAGE_BUILT = robot-framework-dind

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: [arch=arm64] [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	$(eval arch := $(shell if [ -z ${arch} ]; then echo "amd64"; else echo "${arch}"; fi))
	$(eval latest_sha := $(shell docker pull ${IMAGE_FROM} >/dev/null 2>&1 && docker inspect --format='{{index .RepoDigests 0}}' ${IMAGE_FROM}))
	$(eval version := $(shell curl -s https://pypi.org/pypi/robotframework/json | jq -r '.info.version'))
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	if [ "${arch}" = "arm64" ]; then \
		docker buildx build --progress plain --no-cache --platform linux/arm64 --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg RF_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} --push . && \
		docker pull ${tag}; \
	else \
		docker build --no-cache --build-arg IMAGE_FROM_SHA=${latest_sha} --build-arg RF_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} . && \
		docker push ${tag}; \
	fi

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

PHONY: docker-build-push docker-rm docker-rmi


## Tests
## ------
checks: ## Run linter checks
checks:
	$(DOCKER) run -t --rm -v "${PWD}:/mnt" hadolint/hadolint:latest hadolint /mnt/Dockerfile

rf-tests: ## Run robot framework tests. Arguments: [image=jfxs/robot-framework-dind:latest] [name=robot-framework-tests] [tag=arm64]
rf-tests:
	$(eval image := $(shell if [ -z ${image} ]; then echo "jfxs/robot-framework-dind:latest"; else echo "${image}"; fi))
	$(eval name := $(shell if [ -z ${name} ]; then echo "rf-tests"; else echo "${name}"; fi))
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "amd64"; else echo "${tag}"; fi))
	mkdir -p reports
	chmod 777 reports
	@$(DOCKER) run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports ${image} robot --settag ${tag} --name "${name}" --outputdir /reports RF
	chmod 755 reports

PHONY: checks rf-tests

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
